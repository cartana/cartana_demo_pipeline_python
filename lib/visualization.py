import cv2
import ctypes
import numpy as np


def show_image(image, resize=0):
    """" Open an OpenCV visualization window and show image.
     Can specify how the image show be resized. Otherwise it will calculate automatically.

     """

    if not resize:
        user32 = ctypes.windll.user32
        screensize = user32.GetSystemMetrics(1), user32.GetSystemMetrics(0)
        resize = [screensize[i]/image.shape[i] for i in range(0, 2)]
        resize = round(min(resize)*.9, 2)

    image = cv2.resize(image, (0,0), fx=resize, fy=resize)

    cv2.imshow('voila', image/np.max(image))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cv2.waitKey(1)

    return resize


