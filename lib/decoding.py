# base-calling results to gene names
# xiaoyan{at}cartana.se


import numpy as np
from matplotlib import pyplot as plt
import random
import cv2


def get_results(spots, codebook, iss_channels=[1,2,3,4]):
    section_number = len(spots)

    genes = []
    code = []
    with open(codebook, 'r') as f:
        for line in f:
            line = line.rstrip('\n')
            items = line.split(',')
            if items[0] != 'gene':
                genes.append(items[0])
                code.append(int(items[1]))
    genes = np.array(genes, dtype='U256')

    decoded_all_sections = [[], []]

    for s in range(section_number):
        round_number = np.max(spots[s][:,2]) + 1
        base = np.argmax(spots[s][:,5:], axis=1)
        base = np.array(iss_channels)[base]
        _, idx_tile = np.unique(spots[s][:,:2], return_inverse=True, axis=0)
        _, idx_round = np.unique(spots[s][:,2], return_inverse=True)

        basecall = np.empty((int(base.shape[0]/round_number), 4+round_number), dtype='uint16')
        r = 0
        for t in range(np.max(idx_tile)+1):
            nspots = int(np.count_nonzero(idx_tile==t)/round_number)
            basecall[r:r+nspots, :2] = spots[s][np.logical_and(idx_tile==t, idx_round==0), :2]
            basecall[r:r+nspots, 2:4] = spots[s][np.logical_and(idx_tile==t, idx_round==0), 4:2:-1] + \
                                       spots[s][np.logical_and(idx_tile==t, idx_round==0), :2]
            basecall[r:r+nspots, 4:] = np.reshape(base[idx_tile==t], (round_number, -1)).T
            r += nspots

        reads = np.zeros((basecall.shape[0]), dtype='uint32')
        for r in range(round_number):
            reads += basecall[:, 4+r] * 10**(round_number-1 - r)
            # leading 0 in reads will get "lost", but same as when converting code into integer

        match = np.tile([-1], (basecall.shape[0]))
        for c, barcode in enumerate(code):
            match[reads==barcode] = c

        gene_call = np.hstack((basecall[match!=-1, 2:4], reads[match!=-1,None],
                               genes[match[match!=-1],None]))
        decoded_all_sections[0].append(gene_call)
        decoded_all_sections[1].append(np.hstack((basecall, reads[:,None])))

    return decoded_all_sections


def plot_results(array_x_y_name, image):
    image = cv2.imread(image, -1)
    plt.imshow(image)
    name, iName = np.unique(array_x_y_name[:,-1], return_inverse=True)
    color = ['r', 'b', 'g', 'y', 'm', 'c']
    symbol = ['.', 'o', '^', 'v']
    for i in range(max(iName)+1):
    # for i in range(6):
        plt.plot([int(x) for x in array_x_y_name[iName==i,0]],
                 [int(y) for y in array_x_y_name[iName==i,1]],
                 color[random.randint(0,len(color)-1)]+symbol[random.randint(0,len(symbol)-1)])
    plt.show()







