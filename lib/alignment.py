# for initial translation of tissue sections caused by microscope stage movement between imaging rounds
# xiaoyan{at}cartana.se


import os
import numpy as np
import cv2
from lib import imagematrix
from lib import visualization


def pre_align(folder, reference_images, float_images, alignment_channel=5, downscale=10):
    """
    Pre-align sections between imaging rounds and output transformation matrix
    Images are 16-bit.

    Args:
        folder: the parent folder which contains images, output files will be created in the same folder
        reference_images: one DAPI image per section, used as reference, reference_images (section)
        float_images: all images which need to be aligned, float_images (section, round, channel)
        alignment_channel: the imaging channel used for alignment (e.g. DAPI), counting from 1
        downscale: how much the images can be downsized to increase speed
    """

    # get the dimensions
    section_number, round_number, channel_number = float_images.shape

    # create the file which will record translation information and write the header
    with open(os.path.join(folder, 'transformation_matrix.csv'), 'a') as f:
        f.write('image,scale,deltaX,deltaY,response,reference_original_sizeX,reference_original_sizeY\n')

    # align downsized DAPI and save transformation matrix
    for s in range(0, section_number):
        # load reference image
        ref_original = cv2.imread(os.path.join(folder, reference_images[s]), -1)
        refsize = ref_original.shape
        # downscale the image to 10%
        ref_small = cv2.resize(ref_original, (0,0), fx=1/downscale, fy=1/downscale)
        ref_small = ref_small.astype(float) / 65535
        del ref_original

        # loop through imaging rounds
        for r in range(0, round_number):
            if not os.path.isfile(os.path.join(folder, 's' + str(s) + '_r' + str(r) + '.jpg')):
                print(s, r)
                # load floating image
                float_original = cv2.imread(os.path.join(folder, float_images[s,r,alignment_channel-1]), -1)
                # downscale to 10%
                float_small = cv2.resize(float_original, (0,0), fx=1/downscale, fy=1/downscale)
                # pad to the same size as reference
                float_small = imagematrix.pad_image(float_small, list(ref_small.shape))
                del float_original

                # # just to see how it looks before alignment, needs to close the window to continue
                # visualization.show_image(np.dstack((ref_small, float_small, ref_small))/np.max(ref_small))

                float_small = float_small.astype(float)/65535
                transformation = cv2.phaseCorrelate(ref_small, float_small)
                T = np.float32([[1, 0, -transformation[0][0]], [0, 1, -transformation[0][1]]])
                aligned_small = cv2.warpAffine(float_small, T, (ref_small.shape[1], ref_small.shape[0]))

                # write the color image which shows the result of alignment (purple: reference, green: aligned floating)
                cv2.imwrite(os.path.join(folder, 's' + str(s) + '_r' + str(r) + '.jpg'),
                            np.dstack((ref_small, aligned_small, ref_small))/np.max(ref_small)*255)

                # write to file
                with open(os.path.join(folder, 'transformation_matrix.csv'), 'a') as f:
                    f.write('%s,%f,%f,%f,%f,%d,%d\n' % ('s' + str(s) + '_r' + str(r), 1/downscale, transformation[0][0],
                                                  transformation[0][1], transformation[1],
                                                  refsize[1], refsize[0]))


def apply_transform(folder, float_images):
    """ Apply transformation matrices to images """

    if not os.path.isdir(os.path.join(folder, 'AlignedImages')):
        os.makedirs(os.path.join(folder, 'AlignedImages'))

    # channel dimension
    channel_number = float_images.shape[2]

    aligned_images = np.empty((float_images.shape[0], float_images.shape[1], float_images.shape[2]), dtype='U256')

    # open the csv file that stored transformation information
    with open(os.path.join(folder, 'transformation_matrix.csv'), 'r') as f:
        for line in f:
            line = line.rstrip('\n').split(',')
            if line[0] != "image":
                [s, r] = line[0].split('_')
                s = int(s[1])
                r = int(r[1])
                deltaX = float(line[2])/float(line[1])
                deltaY = float(line[3])/float(line[1])
                sizeX = int(line[5])
                sizeY = int(line[6])

                if sizeX > 32767 or sizeY > 32767:
                    print("%s size too big" % line[0])
                else:
                    T = np.float32([[1, 0, -deltaX], [0, 1, -deltaY]])

                    for c in range(0, channel_number):
                        # skip if aligned images already exist
                        if not os.path.isfile(os.path.join(folder, 'AlignedImages', line[0] + '_c' + str(c) + '.tif')):
                            I = cv2.imread(os.path.join(folder, float_images[s,r,c]), -1)
                            I = cv2.warpAffine(I, T, (sizeX, sizeY))
                            cv2.imwrite(os.path.join(folder, 'AlignedImages', line[0] + '_c' + str(c) + '.tif'), I)
                            del I
                        aligned_images[s, r, c] = os.path.join(folder, 'AlignedImages', line[0] + '_c' + str(c) + '.tif')
    return aligned_images


