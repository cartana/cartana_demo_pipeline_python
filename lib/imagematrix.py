# tile images into smaller size for analysis
# xiaoyan{at}cartana.se


import numpy as np
import cv2
import os


def pad_image(image, refsize):
    """ pad/crop 2D images to match the reference size
        refsize: [num_rows, num_cols]
    """

    for d in range(0, 2):
        delta = refsize[d] - image.shape[d]
        if delta > 0:
            pad = np.zeros((delta, image.shape[1-d]), dtype=image.dtype)
            if d:
                pad = pad.T
            image = np.concatenate((image, pad), axis=d)
        elif delta < 0:
            if d:
                image = image[:, 0:delta]
            else:
                image = image[0:delta, :]
    return image


def tile_image_series(images, tile_size):

    for i in images:

        folder, fname = os.path.split(i)
        fname = os.path.splitext(fname)[0]

        if len(i) and os.path.isdir(os.path.join(folder, 'Tiled_' + fname)):
            print('Already tiled ' + fname)
            t = len(os.listdir(os.path.join(folder, 'Tiled_' + fname)))
        else:
            I = cv2.imread(i, -1)

            try:
                tilenum = np.ceil(np.array(I.shape)/2000).astype(int)
                I = pad_image(I, tilenum*2000)

                print('Tiling ' + fname)
                os.makedirs(os.path.join(folder, 'Tiled_' + fname))

                t = 0
                for y in range(0, I.shape[0], tile_size):
                    for x in range(0, I.shape[1], tile_size):
                        t += 1
                        cv2.imwrite(os.path.join(folder, 'Tiled_' + fname, 'tile_' + str(x) + '_' + str(y) + '.tif'),
                                    I[y:y+tile_size, x:x+tile_size])
            except AttributeError:
                t = 0
                print('Could not find ' + fname)
    return t


def tile_images(images, tile_size=2000):
    section_number, round_number, channel_number = images.shape
    tile_numbers = []
    for s in range(0, section_number):
        image_series = []
        for r in range(0, round_number):
            for c in range(0, channel_number):
                image_series.append(images[s,r,c])
        t = tile_image_series(image_series, tile_size)
        tile_numbers.append(t)
    return tile_numbers



