# segment spots and read intensities
# xiaoyan{at}cartana.se

import cv2
import numpy as np
import os
import datetime
from matplotlib import pyplot as plt
import multiprocessing


def segment_spots(image, threshold=0, rcpsize=7):
    """ Segment blobs. Watershed step included. """

    # tophat filter and threshold using the value provided or calculated from Ostu's method
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (rcpsize, rcpsize))
    img_tophat = cv2.morphologyEx(image, cv2.MORPH_TOPHAT, kernel)
    if threshold:
        threshold, img_binary = cv2.threshold(img_tophat, threshold, 1, cv2.THRESH_BINARY)
    else:
        threshold, img_binary = cv2.threshold(img_tophat, 0, 1, cv2.THRESH_OTSU)

    # get the pixels corresponding to intensity max within a 3x3 eclipse
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    img_maxima = np.logical_and(image==cv2.dilate(image, kernel), img_binary.astype(bool))

    # convert maxima into individual components (merge if pixels are connected - important to avoid saturation during imaging)
    # and use as seeds in watershed
    _, img_label = cv2.connectedComponents(img_maxima.astype('uint8'))
    max_labels, max_pixels = np.unique(img_label, return_index=True)

    # label pixels belonging to background=1 (0 in img_label), max pixel of different objects=object number,
    # touching area (to be separated by watershed)=0
    ws_marker = img_label+1
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    touching = cv2.subtract(cv2.dilate(img_binary, kernel), img_maxima.astype('uint16'))  # cv2.dilate does not take binary
    ws_marker[touching==1] = 0

    # watershed (opencv watershed algorithm takes 3-color uint8 images and a marker image of int32 type)
    spots_mask = cv2.watershed(cv2.cvtColor(((65535-img_tophat)/255).astype('uint8'), cv2.COLOR_GRAY2BGR), ws_marker)
    boundaries = spots_mask==-1
    spots_mask = spots_mask-1
    spots_mask[spots_mask<0] = 0

    # remove maxima pixels that in the end do not belong to any spot (0 in mask image)
    # idx = np.reshape(spots_mask, (-1, 1))[max_pixels] != 0
    idx = np.isin(max_labels, spots_mask[spots_mask!=0])
    max_pos = np.asarray(np.unravel_index(max_pixels[idx], image.shape))
    max_pos = max_pos.T

    return spots_mask.astype('uint32'), boundaries, max_pos


def fine_align(ref_image, float_image, apply_on_these):
    # TODO: will probably change to other methods than phase correlation
    transformation = cv2.phaseCorrelate(ref_image.astype('float32'), float_image.astype('float32'))
    T = np.float32([[1, 0, -transformation[0][0]], [0, 1, -transformation[0][1]]])
    for d in range(apply_on_these.shape[2]):
        apply_on_these[:,:,d] = cv2.warpAffine(apply_on_these[:,:,d], T, (ref_image.shape[1], ref_image.shape[0]))

    return apply_on_these


def read_intensities(spots_mask, grayscale_image, rcpsize=7):
    # TODO: find an alternative to speed up
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (rcpsize, rcpsize))
    # tophat on individual channels
    img_tophat = np.empty(grayscale_image.shape, dtype='uint16')
    for c in range(grayscale_image.shape[2]):
        img_tophat[:,:,c] = cv2.morphologyEx(grayscale_image[:,:,c], cv2.MORPH_TOPHAT, kernel)

    max_intensity = np.zeros((len(np.unique(spots_mask))-1, grayscale_image.shape[2]), dtype='uint16')
    for c, i in enumerate(np.unique(spots_mask)):
        if i:
            max_intensity[c-1,:] = np.amax(img_tophat[spots_mask==i,:], axis=0)

    return max_intensity


def process_single_tile(spot_images, round_number, channel_number, tile_id, threshold, rcpsize):

    tile_spots = np.zeros((0, channel_number+5), dtype='uint16')

    for r in range(round_number):
        folder, fname = os.path.split(spot_images[r, 0])
        fname = os.path.splitext(fname)[0]
        img_stack = cv2.imread(os.path.join(folder, 'Tiled_' + fname, 'tile_' + str(tile_id) + '.tif'), -1)
        for c in range(1, channel_number):
            folder, fname = os.path.split(spot_images[r, c])
            fname = os.path.splitext(fname)[0]
            image = cv2.imread(os.path.join(folder, 'Tiled_' + fname, 'tile_' + str(tile_id) + '.tif'), -1)
            img_stack = np.dstack((img_stack, image))

        img_spots_combined = np.amax(img_stack, axis=2)
        if r == 0:  # reference round
            reference = img_spots_combined
            spots_mask, boundaries, max_pos = segment_spots(img_spots_combined, threshold=threshold)
        else:
            img_stack = fine_align(reference, img_spots_combined, img_stack)

        now = datetime.datetime.now()
        print("%s:%s tile-%s round-%r spots-%d" % (now.hour, now.minute, tile_id, r, len(np.unique(spots_mask)) - 1))

        intensities_round = read_intensities(spots_mask, img_stack, rcpsize)

        # pool_in = []
        # for c in range(channel_number):
        #     pool_in.append((spots_mask, img_stack[:,:,c], rcpsize))
        # pool = multiprocessing.Pool(channel_number)
        #
        # for pool_out in pool.map(read_intensities, pool_in):
        #     intensities_round[:,c] = pool_out

        tile_spots = np.vstack((tile_spots,
                                np.hstack((np.tile([int(tile_id.split('_')[0]), int(tile_id.split('_')[1]), r],
                                                   (intensities_round.shape[0], 1)),
                                           max_pos, intensities_round))))

    return tile_spots


def get_spot_intensities(aligned_images, threshold=0, rcpsize=7):
    section_number, round_number, channel_number = aligned_images.shape

    spots_all_sections = []

    for s in range(section_number):
        now = datetime.datetime.now()
        print("%s:%s:%s starting section %d" % (now.hour, now.minute, now.second, s))

        spots = np.zeros((0, channel_number+5), dtype='uint16')

        folder, fname = os.path.split(aligned_images[s,0,0])
        fname = os.path.splitext(fname)[0]

        for tile in os.listdir(os.path.join(folder, 'Tiled_' + fname)):
            if not os.path.isdir(os.path.join(folder, 'Tiled_' + fname, tile)):
                tile_spots = process_single_tile(aligned_images[s,:,:], round_number, channel_number, tile[5:-4],
                                                 threshold, rcpsize)

                # TODO: better way to grow array dynamically. pre-allocate? - how big? write to file?
                spots = np.vstack((spots, tile_spots))

        spots_all_sections.append(spots)
    return spots_all_sections





