# ISS pipeline in python
# The purpose of the pipeline is to help understanding of MATLAB-Cellprofiler pipeline
# NOT recommended to use for a big data set, since no parallelization has been implemented
# Has only been tested with a small data set in debugging mode
# xiaoyan{at}cartana.se


import os
import numpy as np
from lib import alignment
from lib import imagematrix
from lib import segmentation
from lib import decoding

folder = r'DemoData2'   # the root folder where all section images are stored and output will be created
downscale = 10  # down-scale the images for fast transformation during pre-alignment
section_number = 1  # number of sections included in one experiment
round_number = 6    # number of imaging rounds for full decoding
channel_number = 5    # number of fluorescence channels
subfolders = os.listdir(folder)
subfolders = [i + "/export/" for i in subfolders if "Cycle" in i]
codebook = r"DemoData2\codebook.csv"    # the codebook with gene names and color code matching channel order


# path to reference DAPI images, one for each section: reference_images (section)
reference_images = []
for s in range(0, section_number):
    reference_images.append(os.path.join(folder, 'Anchor', 'export', 'anchor' + str(s+1) + 'c2.tif'))

# # check images
# import cv2
# from lib import visualization
# temp = cv2.imread(os.path.join(folder, reference_images[0]))
# visualization.show_image(image)

# all images which need to be aligned: float_images (section, round, channel)
float_images = np.empty((section_number, round_number, channel_number), dtype='U256')
for s in range(section_number):
    for r in range(round_number):
        for c in range(channel_number):
            float_images[s,r,c] = ''.join((subfolders[r], 'Cycle' + str(r+1), '_', str(s+1), 'c', str(c+1), '.tif'))


# START the analysis
# pre-align sections (based on DAPI - channel 5)
alignment.pre_align(folder, reference_images, float_images, alignment_channel=5)
aligned_images = alignment.apply_transform(folder, float_images)

# tile images to smaller size
imagematrix.tile_images(aligned_images, tile_size=2000)

# read intensities for base-calling (skip DAPI channel - last in this case)
spots = segmentation.get_spot_intensities(aligned_images[:,:,:-1], threshold=200)

# decoding
genes = decoding.get_results(spots, codebook, iss_channels=[1,2,3,4])

# plot
for s in range(section_number):
    decoding.plot_results(genes[0][s][:,[0,1,3]], reference_images[s])

# TODO: cv2.imwarp size limit (32767 px in any dimension) workaround (matrix transformation outside OpenCV)
# TODO: parallalization (tile? round?)
# TODO: different method for read_intensities (skimage? just use max position?)
# TODO: some error catching
# TODO(?): input json or csv files with file path


